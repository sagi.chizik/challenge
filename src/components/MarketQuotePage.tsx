import React from 'react';
import { Paper, Toolbar } from '@material-ui/core';
import styled from 'styled-components';
import MarketQuoteInput from './MarketQuoteInput';
import MarketQuoteResults from './MarketQuoteResults';
import MarketQuoteDynamicSwitch from './MarketQuoteDynamicSwitch';

const Page = styled(Paper)`
  padding: 0 1rem 2rem;
`;

const ToolbarStyled = styled(Toolbar)`
  > div {
    display: flex;
    width: 100%;
    justify-content: space-between;
    align-items: center;
  }
`;

export default () => {
  return (
    <Page>
      <ToolbarStyled>
        <div>
          <h3>Buyer Panel</h3>
          <MarketQuoteDynamicSwitch />
        </div>
      </ToolbarStyled>
      <div>
        <MarketQuoteInput />
        <MarketQuoteResults />
      </div>
    </Page>
  );
};
