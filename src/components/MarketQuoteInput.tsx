import React, { useCallback } from 'react';
import { FormControl, Select, MenuItem, TextField, Button, InputLabel } from '@material-ui/core';
import XIcon from '@material-ui/icons/Clear';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import { IStores } from '../logic/stores';

const Row = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 10px;
  > svg {
    margin: 0 10px;
  }
`;

export interface FormData {
  product: string;
  quantity: number;
}

interface Props {
  products?: string[];
  queryMarket?: (product: string, quantity: number) => void;
}

const MarketQuoteInput = ({ products, queryMarket }: Props) => {
  const [formData, setFormData] = React.useState<FormData>({ product: '', quantity: 800 });

  const updateForm = useCallback(
    (field, value) => {
      setFormData({
        ...formData,
        [field]: value,
      });
    },
    [formData]
  );

  const handleChange = (field: string) => (event: React.ChangeEvent<any>) => {
    updateForm(field, event.target.value);
  };

  const handleSubmit = (event: any) => {
    event.preventDefault();
    if (queryMarket) {
      queryMarket(formData.product, formData.quantity);
    } else {
      throw new Error('you shouldnt be here, never, ever');
    }
  };

  return (
    <form>
      <Row>
        <FormControl variant="outlined">
          <InputLabel htmlFor="product-label">Pick a seller...</InputLabel>
          <Select
            id="product-select"
            name="product"
            label="Pick a seller..."
            value={formData.product}
            onChange={handleChange('product')}
            style={{ minWidth: 150 }}
          >
            <MenuItem key={''} value="">
              <em>Pick a seller...</em>
            </MenuItem>
            {(products || []).map(product => (
              <MenuItem key={product} value={product}>
                {product}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <XIcon />
        <FormControl variant="outlined">
          <TextField
            variant="outlined"
            id="quantity"
            label="Quantity"
            type="number"
            inputProps={{
              min: 1,
              max: 10000,
              step: 1,
            }}
            value={formData.quantity}
            onChange={handleChange('quantity')}
            style={{ minWidth: 150 }}
          />
        </FormControl>
        <Button
          variant="contained"
          color="primary"
          onClick={handleSubmit}
          style={{ marginLeft: 10 }}
        >
          Search
        </Button>
      </Row>
    </form>
  );
};

export default inject(({ buyerStore }: IStores) => {
  return {
    products: buyerStore.products,
    queryMarket: buyerStore.queryMarket,
  };
})(observer(MarketQuoteInput));
