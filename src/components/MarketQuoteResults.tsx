import React from 'react';
import {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  Paper,
  TableBody,
} from '@material-ui/core';
import { MarketQueryResult } from '../logic/stores/buyerStore';
import { inject, observer } from 'mobx-react';
import { IStores } from '../logic/stores';
import styled from 'styled-components';
import ReactApexChart from 'react-apexcharts';
import mockSeries from './mock.json';
import { BarChart, Bar } from 'recharts';

const formatPrice = (price: number) => `$${price.toFixed(3)}`;

const TableRowStyled = styled(TableRow)`
  background: #555;
  > th,
  > td {
    color: #fff;
    font-weight: 700;
  }
  * {
    text-transform: uppercase;
  }
`;

const chartOptions = {
  chart: {
    type: 'candlestick',
    height: 350,
  },
  title: {
    text: 'CandleStick Chart',
    align: 'left',
  },
  xaxis: {
    type: 'datetime',
  },
  yaxis: {
    tooltip: {
      enabled: true,
    },
  },
};

interface Props {
  marketQueryResults?: MarketQueryResult[];
  bestPrice?: MarketQueryResult[];
}

const MarketQuoteResults = ({ marketQueryResults, bestPrice }: Props) => {
  return (
    <>
      {marketQueryResults && marketQueryResults.length > 0 && (
        <>
          <br />
          <h3>Sellers</h3>
          <TableContainer component={Paper}>
            <Table aria-label="results table">
              <TableHead>
                <TableRowStyled style={{}}>
                  <TableCell>Seller</TableCell>
                  <TableCell>Product</TableCell>
                  <TableCell align="right">Inventory</TableCell>
                  <TableCell align="right">Price</TableCell>
                  <TableCell>History</TableCell>
                </TableRowStyled>
              </TableHead>
              <TableBody>
                {(marketQueryResults || []).map(row => (
                  <TableRow key={row.id}>
                    <TableCell component="th" scope="row">
                      {row.id}
                    </TableCell>
                    <TableCell>{row.product}</TableCell>
                    <TableCell align="right">{row.quantity}</TableCell>
                    <TableCell align="right">{formatPrice(row.price)}</TableCell>
                    <TableCell>
                      <BarChart
                        width={150}
                        height={35}
                        data={row.history.map(price => ({ price }))}
                        margin={{
                          top: 1,
                          right: 0,
                          left: 0,
                          bottom: 1,
                        }}
                      >
                        <Bar dataKey="price" fill="#8884d8" />
                      </BarChart>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </>
      )}
      {bestPrice && bestPrice.length > 0 && (
        <>
          <br />
          <h3>Best Prices</h3>
          <TableContainer component={Paper}>
            <Table aria-label="results table">
              <TableHead>
                <TableRowStyled>
                  <TableCell>Seller</TableCell>
                  <TableCell>Product</TableCell>
                  <TableCell align="right">Amount</TableCell>
                  <TableCell align="right">Price</TableCell>
                </TableRowStyled>
              </TableHead>
              <TableBody>
                {(bestPrice || []).map(row => (
                  <TableRow key={row.id}>
                    <TableCell component="th" scope="row">
                      {row.id}
                    </TableCell>
                    <TableCell>{row.product}</TableCell>
                    <TableCell align="right">{row.quantity}</TableCell>
                    <TableCell align="right">{formatPrice(row.quantity * row.price)}</TableCell>
                  </TableRow>
                ))}
                <TableRowStyled key="sum">
                  <TableCell colSpan={3}>Best Total Price:</TableCell>
                  <TableCell align="right">
                    <h3>
                      {formatPrice(
                        bestPrice.reduce((acc, best) => acc + best.quantity * best.price, 0)
                      )}
                    </h3>
                  </TableCell>
                </TableRowStyled>
              </TableBody>
            </Table>
          </TableContainer>
        </>
      )}
      <br />
      {marketQueryResults && marketQueryResults.length > 0 && (
        <ReactApexChart
          options={chartOptions}
          series={mockSeries}
          type="candlestick"
          height={350}
        />
      )}
    </>
  );
};

export default inject(({ buyerStore }: IStores) => {
  return {
    marketQueryResults: buyerStore.marketQueryResults,
    bestPrice: buyerStore.bestPrice,
  };
})(observer(MarketQuoteResults));
