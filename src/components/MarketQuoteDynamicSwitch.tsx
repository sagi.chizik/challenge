import React from 'react';
import { Switch, FormControlLabel } from '@material-ui/core';
import { inject, observer } from 'mobx-react';
import { IStores } from '../logic/stores';
import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  align-content: center;
  min-width: 120px;
`;

interface Props {
  dynamic?: boolean;
  setDynamic?: (dynamic: boolean) => void;
}

const MarketQuoteDynamicSwitch = ({ dynamic, setDynamic }: Props) => {
  const handleChange = (event: React.ChangeEvent<any>) => {
    if (setDynamic) {
      setDynamic(event.target.checked);
    }
  };

  return (
    <Container>
      <FormControlLabel
        control={<Switch color="primary" checked={!!dynamic} onChange={handleChange} />}
        label={dynamic ? 'Dynamic' : 'Static'}
      />
    </Container>
  );
};

export default inject(({ buyerStore }: IStores) => {
  return {
    dynamic: buyerStore.dynamic,
    setDynamic: buyerStore.setDynamic,
  };
})(observer(MarketQuoteDynamicSwitch));
