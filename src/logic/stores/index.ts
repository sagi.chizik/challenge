import { BuyerStore } from './buyerStore';

export interface IStores {
  buyerStore: BuyerStore;
}

export default {
  BuyerStore,
};
