import { action, observable, computed } from 'mobx';
import marketplaceData from '../../data/marketplaceData.json';
import { Market } from '../market/Market';
import { Seller, Inventory, InventoryItemBase } from '../market/Seller';
import { Subscription } from 'rxjs';

interface LoadedData {
  name: string;
  wait: number;
  inventory: Inventory<InventoryItemBase>;
}

export interface MarketQuery {
  product: string;
  quantity: number;
}

export interface MarketQueryResult {
  id: string;
  product: string;
  quantity: number;
  price: number;
  history: number[];
}

export class BuyerStore {
  private tickSubscription?: Subscription;
  @observable dynamic: boolean = false;
  @observable market: Market = new Market([]);
  @observable query: MarketQuery = { product: '', quantity: 100 };

  @observable marketQueryResults: MarketQueryResult[] = [];

  constructor(numOfSellers = 3) {
    this.initMarket(numOfSellers);
  }

  @computed
  get products() {
    const names = this.market.sellers.reduce<Set<string>>((acc, seller) => {
      Object.entries(seller.inventory).forEach(([productName, value]) => {
        acc.add(productName);
      });
      return acc;
    }, new Set<string>());
    return Array.from(names).sort((a, b) => (a > b ? 1 : -1));
  }

  @computed
  get bestPrice() {
    let quantity = this.query.quantity;
    const best = this.marketQueryResults.reduce<MarketQueryResult[]>((acc, marketQueryResult) => {
      if (quantity <= 0) {
        return acc;
      }
      const amnt = Math.min(quantity, marketQueryResult.quantity);
      quantity -= amnt;
      const q: MarketQueryResult = {
        ...marketQueryResult,
        quantity: amnt,
      };
      return [...acc, q];
    }, []);
    return best;
  }

  @action.bound
  initMarket(numOfSellers: number = 3) {
    if (this.tickSubscription) {
      this.tickSubscription.unsubscribe();
    }
    const numBound = Math.max(0, Math.min(marketplaceData.length, numOfSellers));
    const sellers: Seller[] = [];
    for (let i = 0; i < numBound; i += 1) {
      const { name, wait, inventory } = marketplaceData[i] as LoadedData;
      sellers.push(new Seller(inventory, name, wait));
    }
    this.market = new Market(sellers);
    this.tickSubscription = this.market.observable.subscribe({
      next: () => {
        if (this.dynamic) {
          this.queryMarket(this.query.product, this.query.quantity);
        }
      },
    });
  }

  @action.bound
  setDynamic(dynamic: boolean = true) {
    this.dynamic = dynamic;
  }

  @action.bound
  queryMarket(product: string, quantity: number) {
    if (!product) return;

    this.query = { product, quantity };

    const results = this.market.sellers.map<MarketQueryResult>(seller => {
      const product = seller.inventory[this.query.product];
      return {
        id: seller.id,
        product: this.query.product,
        quantity: product?.quantity || 0,
        price: product?.price || 0,
        history: product.priceHistory,
      };
    });

    this.marketQueryResults = results
      .filter(result => result.quantity > 0)
      .sort((a, b) => (a.price > b.price ? 1 : -1));
  }
}
