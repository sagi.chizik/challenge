import { create as createSeed, RandomSeed } from 'random-seed';

export interface InventoryItemBase {
  quantity: number;
  price: number;
}

export interface InventoryItem extends InventoryItemBase {
  startingQuantity: number;
  priceHistory: number[];
  stingyness: number;
}

export interface Inventory<T = InventoryItem> {
  [K: string]: T;
}

type GeneratorFunc = (n: number) => number;

const getExpectedChange = (generator: GeneratorFunc) => generator(100) / 100;

const getDeliveries = (inventoryItem: InventoryItem, randomSeed: RandomSeed): number => {
  const fluctuation = getExpectedChange(randomSeed);
  const newDeliveries = fluctuation * inventoryItem.startingQuantity;
  return Math.floor(inventoryItem.quantity + newDeliveries);
};

const calculatePriceChange = (inventoryItem: InventoryItem, randomSeed: RandomSeed) => {
  const v = 0.1;
  const ec = getExpectedChange(randomSeed);
  const alpha = inventoryItem.startingQuantity;
  const beta = inventoryItem.quantity;
  const inv_based_change = Math.log10(beta / alpha) * -v;
  const sentimentChange = inv_based_change + (ec - 0.5) * v;
  return sentimentChange;
};

export class Seller {
  public id: string;
  private deliveryWait: number;
  private randomSeed: RandomSeed;
  public inventory: Inventory<InventoryItem>;

  constructor(inventory: Inventory<InventoryItemBase>, id = 'Safeway', deliveryWait = 5) {
    this.inventory = Object.entries(inventory).reduce<Inventory>(
      (acc, [key, itemBase]) => ({
        ...acc,
        [key]: {
          ...itemBase,
          startingQuantity: itemBase.quantity,
          priceHistory: [itemBase.price],
          stingyness: 0,
        },
      }),
      {}
    );

    this.deliveryWait = deliveryWait;
    this.randomSeed = createSeed(id);
    this.id = id;
  }

  quote(productKey: string) {
    const inventory = this.inventory[productKey];
    return inventory.price;
  }

  sell(productKey: string, buyQuantity: number) {
    const inventoryItem = this.inventory[productKey];
    const boughtQuantity =
      buyQuantity > inventoryItem.quantity ? inventoryItem.quantity : buyQuantity;
    const cost = boughtQuantity * this.quote(productKey);
    inventoryItem.quantity -= boughtQuantity;
    inventoryItem.stingyness = 1 - inventoryItem.quantity / inventoryItem.startingQuantity;
    this.tick();
    return { boughtQuantity, cost };
  }

  tick() {
    for (let [productKey, inventoryItem] of Object.entries(this.inventory)) {
      let quantity = inventoryItem.quantity;
      const isReadyForDelivery = inventoryItem.priceHistory.length % this.deliveryWait === 0;
      if (isReadyForDelivery) {
        quantity = getDeliveries(inventoryItem, this.randomSeed);
      }
      const chg = calculatePriceChange(inventoryItem, this.randomSeed);
      const newPrice = inventoryItem.price + inventoryItem.price * chg;
      const modifiedItem = {
        ...inventoryItem,
        price: newPrice,
        priceHistory: [...inventoryItem.priceHistory, newPrice],
        quantity,
      };
      this.inventory[productKey] = modifiedItem;
    }
  }
}
