import { Market } from './Market';

export class Buyer {
  private market: Market;

  constructor(market: Market) {
    this.market = market;
  }

  getBestPrice(productKey: string) {
    throw Error('Not Implemented');
  }

  completelyFill(productKey: string, quantity: number) {
    throw Error('Not Implemented');
  }

  quicklyFill(productKey: string, quantity: number) {
    throw Error('Not Implemented');
  }
}
