import { interval, Subject } from 'rxjs';
import { Seller } from './Seller';

export class Market {
  public sellers: Seller[] = [];
  public observable: Subject<any>;

  constructor(sellers: Seller[]) {
    this.sellers = sellers;
    this.observable = new Subject();
    this.observable.subscribe({
      next: () => this.tick(),
    });
    interval(5000).subscribe(v => this.observable.next(v));
  }

  tick() {
    this.sellers.forEach(seller => {
      seller.tick();
    });
  }
}
