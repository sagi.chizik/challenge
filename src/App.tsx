import React from 'react';
import { Provider } from 'mobx-react';
import { ThemeProvider, createMuiTheme, AppBar } from '@material-ui/core';
import MarketQuotePage from './components/MarketQuotePage';
import { BuyerStore } from './logic/stores/buyerStore';
import styled from 'styled-components';
import MillTechLogo from './components/assets/logo.png';

const theme = createMuiTheme({
  palette: {
    secondary: {
      main: '#fefefe',
    },
  },
  typography: {
    fontFamily:
      '"Muli", sans-serif, -apple-system, BlinkMacSystemFont, "Segoe UI","Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans","Helvetica Neue"',
    fontSize: 12,
  },
});

const buyerStore = new BuyerStore();

const AppBarStyled = styled(AppBar)`
  padding: 1rem 2rem;
  height: 80px;
  > * {
    max-height: 100%;
    display: flex;
    align-items: center;
    > img {
      width: auto;
      height: 100%;
    }
  }
`;

const Content = styled.div`
  padding: 7rem 1rem 3rem;
`;

const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <Provider buyerStore={buyerStore}>
        <AppBarStyled color="secondary">
          <div>
            <img src={MillTechLogo} alt="" />
          </div>
        </AppBarStyled>
        <Content>
          <MarketQuotePage />
        </Content>
      </Provider>
    </ThemeProvider>
  );
};

export default App;
